package zk101.ctrl;


import java.util.List;

import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.*;
import org.zkoss.zul.*;

import zk101.model.Car;
import zk101.model.CarService;
import zk101.model.CarServiceImpl;

public class SearchController extends SelectorComposer<Window> {

	private static final long serialVersionUID = 1L;
	
	@Wire
	private Textbox keywordBox;
	@Wire
	private Listbox carListbox;
	@Wire
	private Label errorMsg ,nameLabel, companyLabel, priceLabel, descriptionLabel;
	@Wire("window > hlayout > image")
	private Image previewImage;
	@Wire("hlayout")
	private Hlayout carDetailArea;
	
	private CarService carService = new CarServiceImpl();
	
	
	//A less convenient alternative to using @Listen:
//	public void doAfterCompose(Window comp) throws Exception{
//		super.doAfterCompose(comp);
//		keywordBox.addEventListener("onOK", new EventListener<Event>(){
//			@Override
//			public void onEvent(Event event) throws Exception {
//				String keyword = keywordBox.getValue();
//				List<Car> result = carService.search(keyword);
//				carListbox.setModel(new ListModelList<Car>(result));
//				toggleVisiblityPerSearch(result.isEmpty());
//			}
//		});
//	}
	
	@Listen("onClick = #searchButton ; onOK = #keywordBox")
	public void search(){
		String keyword = keywordBox.getValue();
		List<Car> result = carService.search(keyword);
		carListbox.setModel(new ListModelList<Car>(result));
		toggleVisiblityPerSearch(result.isEmpty());
	}
	
	@Listen("onSelect = #carListbox")
	public void showDetail(){
		carDetailArea.setVisible(true);
		Car selected = carListbox.getSelectedItem().getValue();
		previewImage.setSrc(selected.getPreview());
		nameLabel.setValue(selected.getName());
		companyLabel.setValue(selected.getCompany());
		priceLabel.setValue(selected.getPrice().toString());
		descriptionLabel.setValue(selected.getDescription());
	}
	
	private void toggleVisiblityPerSearch(boolean isCarListEmpty){
		if (isCarListEmpty){
			errorMsg.setVisible(true);
			carListbox.setVisible(false);
			carDetailArea.setVisible(false);
		} else {
			errorMsg.setVisible(false);
			carListbox.setVisible(true);
			carDetailArea.setVisible(false);
		}
	}
}
