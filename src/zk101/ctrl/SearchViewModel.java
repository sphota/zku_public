package zk101.ctrl;

import java.util.List;
import org.zkoss.bind.annotation.*;

import zk101.model.Car;
import zk101.model.CarService;
import zk101.model.CarServiceImpl;

public class SearchViewModel {
	
	private String keyword;
	private List<Car> carList;
	private Car selectedCar;
	private boolean resultEmpty = false;
	
	private CarService carService = new CarServiceImpl();
	
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getKeyword() {
		return keyword;
	}
	
	public List<Car> getCarList(){
		return carList;
	}

	public boolean isResultEmpty(){
		return resultEmpty;
	}
		
	public void setSelectedCar(Car selectedCar) {
		this.selectedCar = selectedCar;
	}
	public Car getSelectedCar() {
		return selectedCar;
	}
	
	@Command
	@NotifyChange({"carList","resultEmpty"})
	public void search(){
		carList = carService.search(keyword);
		resultEmpty = carList.isEmpty();
		selectedCar = null;
	}
	
}
